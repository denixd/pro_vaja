#include <iostream>
#include <random>
#include <vector>
#include "cec19_test_func.cpp"
#include <chrono>
#include <unordered_map>
#include <algorithm>
#include <cmath>

double getMin(std::unordered_map<double*, double>);
double getMax(std::unordered_map<double*, double> );
double getAvg(std::unordered_map<double*, double> );
double getStdDev(std::unordered_map<double*, double>fValues);

int main(int argc, char *argv[]) {

    if(argc != 2) {
        std::cout << "Too few arguments...max_nfes not set...\n";
        return -1;
    }

    int *numArr = new int[10];
    numArr[0] = 3;    

    const int Np = 100;      //  Number of populations
    const int D = 9;       //  Dimension
    const float Cr = 0.9;      //  Constant
    const int F = 0.5;       //  Constant
    const int Bmin = -8192;
    const int Bmax = 8192;

    std::random_device random_device;
    std::mt19937 gen(random_device());
    std::uniform_real_distribution<> dis(0, 1);
    std::uniform_real_distribution<> dis2(0, Np-1);
    std::uniform_real_distribution<> dis3(0, D-1);

    int max_nfes = std::stoi(argv[1]);
    std::cout << "Max_nfes: " << max_nfes << "\n";

    // Variables
    std::vector<double*> population;
    population.reserve(Np);
    double* M = new double[D];
    double* K = new double[D];
    double result0;
    double result1;
    int nfes = 0;
    int call = 0;
    double r1, r2, r3;
    std::unordered_map<double*, double> functionValues;
    functionValues.reserve(max_nfes);

    int individualIndex = 0;

    for(int i = 0; i < Np; i++) {
        population[i] = new double[D];
    }

    // Start timer
    auto start_time = std::chrono::steady_clock::now();

    // Initialization
    for(int i = 0; i < Np; i++) {
        population[i] = new double[D];
        for(int j = 0; j < D; j++){
            population[i][j] = Bmin + (Bmax - Bmin) * dis(gen);
        }
    }

    while (nfes < max_nfes) {
        for (int i = 0; i < Np; i++) {
            individualIndex = i;

            // Mutation
            r1 = dis2(gen);
            r2 = dis2(gen);
            r3 = dis2(gen);

            while(r1 == i) {
                r1 = dis2(gen);
            }

            while(r2 == i || r2 == r1){
                r2 = dis2(gen);
            }

            while(r3 == i || r3 == r2 || r3 == r1){
                r3 = dis2(gen);
            }

            for(int j = 0; j < D; j++){
                M[j] = population[r1][j] + F * (population[r2][j] - population[r3][j]);
            }

            //  Crossing
            int rand_j = dis3(gen);

            for(int j = 0; j < D; j++){
                if(dis(gen) < Cr || rand_j == j){
                    K[j] = M[j];
                } else {
                    K[j] = population[individualIndex][j];
                }
            }

            double* newIndividual = new double[D];

            //  Fixing
            for(int j = 0; j < D; j++) {
                if(K[j] < Bmin) {
                    newIndividual[j] = Bmin + (Bmin - K[j]);
                } else if (K[j] > Bmax){
                    newIndividual[j] = Bmax - (K[j] - Bmax);
                } else {
                    newIndividual[j] = K[j];
                }
            }

            //  Selection

            if(functionValues.find(population[individualIndex]) != functionValues.end()){
                result0 = functionValues[population[individualIndex]];
            } else {
                cec19_test_func(population[individualIndex], &result0, D, 1, 1);
                functionValues[population[individualIndex]] = result0;
                call++;
            }

            if(functionValues.find(newIndividual) != functionValues.end()){
                result1 = functionValues[newIndividual];
            } else {
                cec19_test_func(newIndividual, &result1, D, 1, 1);
                functionValues[newIndividual] = result1;
                call++;
            }

            if(result1 < result0) {
                population[individualIndex] = newIndividual;
            }

            nfes++;
        }
    }

    auto end_time = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

    std::cout << "Duration: " << duration.count() << "ms\n";

    std::cout << "MIN: " << getMin(functionValues) << "\n";
    std::cout << "MAX: " << getMax(functionValues) << "\n";
    std::cout << "AVG: " << getAvg(functionValues) << "\n";
    std::cout << "STD_DEV: " << getStdDev(functionValues) << "\n";

    std::cout << "Evaluations: " << call << "\n";

    // free memory
    delete[] M;
    delete[] K;
    delete numArr;
    for(int i = 0; i < Np; i++) {
        delete population[i];
    }
    return 0;
}

typedef std::pair<double*, double> MyPair;
struct CompareSecondMin {
    bool operator()(const MyPair& left, const MyPair& right) const
    {
        return left.second < right.second;
    }
};

struct CompareSecondMax {
    bool operator()(const MyPair& left, const MyPair& right) const
    {
        return left.second > right.second;
    }
};


double getMin(std::unordered_map<double*, double>fValues) {
    std::pair<double*, double> min = *std::min_element(fValues.begin(), fValues.end(), CompareSecondMin());
    return min.second;
}

double getMax(std::unordered_map<double*, double>fValues) {
    std::pair<double*, double> max = *std::min_element(fValues.begin(), fValues.end(), CompareSecondMax());
    return max.second;
}

double getAvg(std::unordered_map<double*, double>fValues){
    double sum = 0;

    for(auto value : fValues){
        sum += value.second;
    }

    return(sum / fValues.size());
}

double getStdDev(std::unordered_map<double*, double>fValues) {
    double sum = 0.0, variance = 0.0;

    for(auto value : fValues){
        sum += value.second;
    }
    double mean = sum/fValues.size();


    for(auto value : fValues){
        variance += pow(value.second - mean, 2);
    }
    variance /= fValues.size();
    return sqrt(variance);
}
