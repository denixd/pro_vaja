# Povemo kateri image naj vzame iz hub.docker.com
FROM ubuntu:20.04

# Ukaz za prevajalnik
RUN apt-get update && apt-get -y install build-essential

# Instalacija valgrind in cppcheck
RUN apt-get install -y valgrind
RUN apt-get install -y cppcheck
